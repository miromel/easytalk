$(function () {

    "use strict";

    //===== Show iOS app button or Android

    $(window).on('load', function (event) {
        const isIos = /iP(ad|od|hone)/i.test(navigator.userAgent);
        const isAndroid = /Android/i.test(navigator.userAgent);
        let ios = document.getElementById('ios');
        let android = document.getElementById('android');
        if (isAndroid) {
            ios.hidden = true;
        }
        if (isIos) {
            android.hidden = true;
        }
    });


    //===== Sticky

    $(window).on('scroll', function (event) {
        var scroll = $(window).scrollTop();
        if (scroll < 200) {
            $(".navigation").removeClass("sticky");
        } else {
            $(".navigation").addClass("sticky");
        }
    });



    //===== Mobile Menu 

    $(".navbar-toggler").on('click', function () {
        $(this).toggleClass('active');
    });

    $(".navbar-nav a").on('click', function () {
        $(".navbar-toggler").removeClass('active');
    });



    $('a[href*="#_"]').click(function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 1500);
        return false;
    });


    //===== Wow animation js

    new WOW().init();


});
//# sourceMappingURL=app.js.map
